class @Coloring
	constructor: () ->
		@colorThief = new ColorThief.colorRob();

	dominant: (url, session_var) ->
		self = this
		img = new Image()
		img.onload = () ->
			colors = self.colorThief.getPalette(img, 2)
			palette = []
			_.each(colors, (ary) ->
				palette.push(self.convert(ary))
			)
			Session.set(session_var, palette)
		
		img.crossOrigin = 'Anonymous'
		img.src = url

	convert: (ary) ->
		r = @toHex(ary[0])
		g = @toHex(ary[1])
		b = @toHex(ary[2])
		"##{r}#{g}#{b}"

		

	toHex: (c) ->
		hex = c.toString(16);
		if hex.length == 1
			"0" + hex
		else
			hex