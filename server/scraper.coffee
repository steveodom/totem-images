class @Scraper
	constructor: (@future) ->
		Xray = Meteor.npmRequire('x-ray');
		@x = Xray()


	twitter: (handle) ->
		self = this
		url = "http://twitter.com/#{handle}"
		@x(url, {
			header: '.ProfileCanopy-headerBg img@src',
			thumbnail: '.ProfileAvatar-image@src',
			domain: '.ProfileHeaderCard-urlText a'
		})( (err, obj) ->
			if err
				console.info(err, "there has been an error")
			if obj.domain
				obj.domain = obj.domain.trim()
			self.future.return(obj);
		)
		

