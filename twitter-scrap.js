if (Meteor.isClient) {
  Session.set('header', '')
  Session.set('thumbnail', '')
  Session.set('results', false)
  Session.set('isLoading', false)
  Session.set('clearbit', '')
  

  Template.header.helpers({
    header: function () {
      return Session.get('header')
    },
    isLoading: function () {
      return Session.get('isLoading')
    }
  });

  Template.thumbnail.helpers({
    thumbnail: function () {
      return Session.get('thumbnail')
    },
    clearbit: function () {
      return Session.get('clearbit')
    },
    hasClearbit: function () {
      return !_.isEmpty(Session.get('clearbit'))
    }
  });

  Template.form.helpers({
    isVisible: function () {
      if (Session.get('results')) {

      } else {
       return "hidden" 
      }

    },
    header: function () {
      return Session.get('header')
    },

    thumbnail: function () {
      return Session.get('thumbnail')
    },
    clearbit: function () {
      return Session.get('clearbit')
    },
    hasClearbit: function () {
      return !_.isEmpty(Session.get('clearbit'))
    },

    hasColors: function () {
      return !_.isEmpty(Session.get('headerColors')) || !_.isEmpty(Session.get('thumbnailColors')) || !_.isEmpty(Session.get('clearbitColors'))
    },
    headerColors: function () {
      return Session.get('headerColors')
    },
    thumbnailColors: function () {
      return Session.get('thumbnailColors')
    },
    clearbitColors: function () {
      return Session.get('clearbitColors')
    },
    getDominantColor: function () {
      color = "#2196F3"
      if (Session.get('clearbitColors') ) {
        color = Session.get('clearbitColors')[0]
      }

      if (Session.get('headerColors') ) {
        color = Session.get('headerColors')[0]
      }

      return "background-color: " + color;
    }
  });

  Template.form.events({
    'click [data-behavior=click-to-select]': function (event) {
      $(event.currentTarget).select()
    },
    'click button': function (event) {
      event.preventDefault()
      Session.set('isLoading', true)
      handle = $('#twitter').val()
      Meteor.call("scrapContent", handle, function (err, res) {
        Session.set('header', res.header)
        Session.set('thumbnail', res.thumbnail)
        Session.set('results', true)
        Session.set('isLoading', false)


        
        Materialize.fadeInImage('#header')
        Materialize.fadeInImage('#thumbnail')

        if (res.domain) {
          Session.set('clearbit', "https://logo.clearbit.com/" + res.domain + "?size=200")
          Materialize.fadeInImage('#clearbit')
        }

        coloring = new Coloring()
        coloring.dominant(res.header, "headerColors")
        coloring.dominant(res.thumbnail, "thumbnailColors")

        if (res.domain)  {
          coloring.dominant(Session.get('clearbit'), "clearbitColors")
        }
        
      })
    }
  });
}
